def score(probe_one, probe_two, real_one, real_two)
  dif1 = probe_one - probe_two
  dif2 = real_one - real_two
  case
  when dif1 * dif2 > 0
    res = '0'
  when (dif1 == dif2) && (probe_one == real_one)
    res = '1'
  when dif1 * dif2 < 0 
    res = '-1'
  end
  puts res
end
